## Pet feeding beacon

A simple push button and a couple LEDs hooked to an ATtiny402,
to indicate that our dog has already been fed.

Some features:

- Periodic interrupt
- Pin change interrupt
- Power down sleep mode for low power
- LED pulsing for low power
- EEPROM logging
- Battery voltage monitoring
- Easy to understand code with comments

![Blinking LED](Burst_Cover_GIF_Action_20200208171255.gif)

![Schematic](Schematic.png)
